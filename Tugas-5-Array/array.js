//Soal nomer 1
function Range(startNum, finishNum) {
    let numbers = [];
    if (!startNum || !finishNum) {
        return -1;
    } else if (startNum < finishNum) {
        for (let position = startNum; position <= finishNum; position++) {
            numbers.push(position);
        }
    } else if (startNum > finishNum) {
        for (let position = startNum; position >= finishNum; position--) {
            numbers.push(position);
        }
    }
    return numbers;
}
console.log(Range(1, 10));
console.log(Range(1));
console.log(Range(11, 18));
console.log(Range(54, 50));
console.log(Range());

console.log('\n');
//Soal nomer 2
function RangeWithStep(startNum, finishNum, step) {
    let numbers = [];
    if (!startNum || !finishNum) {
        return -1;
    } else if (startNum < finishNum) {
        for (let position = startNum; position <= finishNum;) {
            numbers.push(position);
            position += step;
        }
    } else if (startNum > finishNum) {
        for (let position = startNum; position >= finishNum;) {
            numbers.push(position);
            position -= step;
        }
    }
    return numbers;
}
console.log(RangeWithStep(1, 10, 2));
console.log(RangeWithStep(11, 23, 3));
console.log(RangeWithStep(5, 2, 1));
console.log(RangeWithStep(29, 2, 4));

console.log('\n');
//Soal nomer 3
function Sum(startNum, finishNum, step) {
    let result = 0;
    if (!step) {
        step = 1;
    }
    if (!startNum && finishNum) {
        return finishNum;
    } else if (startNum && !finishNum) {
        return startNum;
    } else if (startNum < finishNum) {
        for (let position = startNum; position <= finishNum;) {
            result += position;
            position += step;
        }
    } else if (startNum > finishNum) {
        for (let position = startNum; position >= finishNum;) {
            result += position
            position -= step;
        }
    }
    return result;
}
console.log(Sum(1, 10)); // 55
console.log(Sum(5, 50, 2)); // 621
console.log(Sum(15, 10)); // 75
console.log(Sum(20, 10, 2)); // 90
console.log(Sum(1)); // 1
console.log(Sum()); // 0 

console.log('\n');
//Soal nomer 4
function DataHandling(data) {
    if (!data) {
        return 'No Data';
    }

    let result = '';
    if (data.length > 0) {
        for (let position = 0; position < data.length; position++) {
            // console.log(data[position].length);
            result += 'Nomor ID: ' + data[position][0] + '\n' +
                'Nama Lengkap: ' + data[position][1] + '\n' +
                'TTL:' + data[position][2] + '\n' +
                'Hobi:' + data[position][3] + '\n';
            result += '\n';
        }
        return result;
    } else {
        return 'No Data';
    }
}
let input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];
console.log(DataHandling(input));

console.log('\n');
//Soal nomer 5
function BalikKata(data) {
    let result = '';
    for (let pos = data.length - 1; pos >= 0; pos--) {
        result += data[pos];
    }
    return result;
}
console.log(BalikKata("Kasur Rusak"));
console.log(BalikKata("SanberCode"));
console.log(BalikKata("Haji Ijah"));
console.log(BalikKata("racecar"));
console.log(BalikKata("I am Sanbers"));

console.log('\n');
//Soal nomer 6
function DataHandling2(data) {
    data[4] = 'Pria';
    data.push('SMA Internasional Metro');
    data[1] += 'Elsharawy';
    let newProvinsi = 'Provinsi ';
    let oldProvinsi = data[2];
    data[2] = newProvinsi + oldProvinsi;
    console.log(data);
    let dates = data[3].split('/');
    let month = parseInt(dates[1]);
    switch (month) {
        case 1:
            console.log('Januari');
            break;
        case 2:
            console.log('Februari');
            break;
        case 3:
            console.log('Maret');
            break;
        case 4:
            console.log('April');
            break;
        case 5:
            console.log('Mei');
            break;
        case 6:
            console.log('Juni');
            break;
        case 7:
            console.log('Juli');
            break;
        case 8:
            console.log('Agustus');
            break;
        case 9:
            console.log('Septembe');
            break;
        case 10:
            console.log('Oktober');
            break;
        case 11:
            console.log('November');
            break;
        case 12:
            console.log('Desember');
            break;
        default:
            console.log('Bulan tidak dapat dikenali');

    }
    for (let i = 0; i < dates.length; i++) {
        dates[i] = dates[i] * 1;
    }
    dates = dates.sort();
    for (let i = 0; i < dates.length; i++) {
        dates[i] = dates[i].toString();
        if (dates[i].length < 2) {
            dates[i] = '0' + dates[i];
        }
    }
    console.log(dates);
    let newArrayDates = [];
    newArrayDates.push(dates[1]);
    newArrayDates.push(dates[2]);
    newArrayDates.push(dates[0]);
    let newDate = newArrayDates.join('-');
    console.log(newDate);
    let arrayName = data[1].split("");
    arrayName = arrayName.slice(0,14);
    let newName = arrayName.join("");
    console.log(newName);


}
let dataInput = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
DataHandling2(dataInput);


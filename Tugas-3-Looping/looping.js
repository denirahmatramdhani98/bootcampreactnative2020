//Soal nomer 1
var increment = 2;
var position = 0;
var firstMessage = 'I love coding';
var secondMessage = 'I Will become a mobile developer';
var firstHeaderMessage = 'LOOPING PERTAMA';
var secondHeaderMessage = 'LOOPING KEDUA';
position = position + increment;
console.log(firstHeaderMessage);
while (position <= 20) {
    console.log(position + ' - ' + firstMessage);
    position = position + increment;
}
console.log(secondHeaderMessage);
position = position - increment;
while (position >= 2) {
    console.log(position + ' - ' + secondMessage);
    position = position - increment;
}

console.log('\n');
//Soal nomer 2
//angka ganjil santai
//angka genap berkualitas
//kelipatan 3 dan ganjil tampilkan i love coding
var message = "";
var limit = 30;
for (var angka = 1; angka <= limit; angka++) {
    message = angka;
    if (angka % 2 == 0) {
        message += ' - Berkualitas';
    } else {
        if (angka % 3 == 0) {
            message += ' - I Love coding';
        } else {
            message += ' - Santai';
        }
    }
    console.log(message);
}

console.log('\n');
//Soal nomer 3
var limitColumn = 8;
var limitRow1 = 4;
for (var row = 1; row <= limitRow1; row++) {
    var draw = "";
    for(var column = 1; column<= limitColumn; column++){
        draw += "#";
    }
    console.log(draw);
}

console.log('\n');
//Soal nomer 4
var limitRow2 = 7;
for (var row = 1; row <= limitRow2; row++) {
    var draw = '';
    for (var x = 1; x <= row; x++) {
        draw += '#';
    }
    console.log(draw);
}

console.log('\n');
//Soal nomer 5
//Membuat papan catur 8x8
var limitRow3 = 8;
var limitCol = 8;
for (var row = 1; row <= limitRow3; row++) {
    var draw = "";
    for (var col = 1; col <= limitCol; col++) {
        switch (row % 2) {
            case 0:
                if (col % 2 == 0) {
                    draw += ' ';
                } else {
                    draw += '#';
                }
                break;
            case 1:
                if (col % 2 == 0) {
                    draw += '#';
                } else {
                    draw += ' ';
                }
                break;
        }
    }
    console.log(draw);
}

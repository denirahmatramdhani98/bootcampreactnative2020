//Soal nomer 1
function ArrayToObject(data) {
    let result = [];
    for (let i = 0; i < data.length; i++) {
        let now = new Date();
        let yearNow = now.getFullYear();
        let temp = {
            firstName: data[i][0],
            lastName: data[i][1],
            gender: data[i][2],
            age: yearNow - (data[i][3] * 1)
        }
        let res = i + 1 + '. ' + data[i][0] + ' ' + data[i][1];
        result[res] = temp;
    }
    console.log(result);
}

let input = [
    ['Abduh', 'Muhammad', 'male', '1992'],
    ['Ahmad', 'Taufik', 'male', '1985']
];
ArrayToObject(input);

console.log('\n');
//Soal nomer 2
function ShoppingTime(memberId, money) {
    if (typeof memberId == 'undefined' || memberId == '') {
        console.log('Mohon maaf, toko X hanya berlaku untuk member saja');
        return;
    }
    if (money >= 50000) {
        let transaction = {
            MemberID: memberId,
            Money: money,
            ListPurchased: [],
            ChangeMoney: 0
        }
        let currMoney = money;
        itemOnSale = itemOnSale.sort((a, b) => b.Price - a.Price);
        itemOnSale.forEach(function (x) {
            if (currMoney >= x.Price) {
                transaction.ListPurchased.push(x.Name);
                transaction.ChangeMoney = currMoney - x.Price;
                currMoney = transaction.ChangeMoney;
            }
        });
        console.log(transaction);
    } else {
        console.log('Mohon maaf, uang tidak cukup');
        return;
    }
}
let itemOnSale = [
    {
        Name: 'Baju Zoro',
        Price: 500000
    },
    {
        Name: 'Baju H&N',
        Price: 250000
    },
    {
        Name: 'Sweater Uniklooh',
        Price: 175000
    },
    {
        Name: 'Casing Handphone',
        Price: 50000
    },
    {
        Name: 'Sepatu Stacattu',
        Price: 1500000
    },
];
ShoppingTime('1820RzKrnWn08', 2475000);
ShoppingTime('82Ku8Ma742', 170000);
ShoppingTime('', 2475000);
ShoppingTime('234JdhweRxa53', 15000);
ShoppingTime();

console.log('\n');
//Soal nomer 3
function NaikAngkot(listPenumpang) {
    let cost = 2000;
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    let listData = [];
    for (let i = 0; i < listPenumpang.length; i++) {
        let dataTemplate = {};
        dataTemplate.Penumpang = listPenumpang[i][0];
        dataTemplate.NaikDari = listPenumpang[i][1];
        dataTemplate.Tujuan = listPenumpang[i][2];
        let indexEnd = rute.indexOf(dataTemplate.Tujuan);
        let indexStart = rute.indexOf(dataTemplate.NaikDari);
        let far = 0;
        if(indexEnd > indexStart){
            far = indexEnd - indexStart;
        } else {
            far = indexStart - indexEnd;
        }
        dataTemplate.Bayar = far * cost;
        listData.push(dataTemplate);
    }
    console.log(listData);
}
NaikAngkot([
    ['Dimitri', 'B', 'F'],
    ['Icha', 'A', 'B'],
    ['Alif', 'F', 'A']
])
NaikAngkot([]);

var nama = "John";
var peran = "guard";
var welcomeMessage = 'Selamat datang di Dunia Werewolf, ';
var message = "";
//output
if ((nama == '' || nama == ' ') && (peran == '' || peran == ' ')) {
    message = 'Nama harus diisi!!';
} else if (nama != '') {
    welcomeMessage += nama;
    message = 'Halo ' + nama + ', ';
    if (peran == '') {
        message += 'Pilih peranmu untuk memulai game!!';
    } else if (peran.toLowerCase() == 'penyihir') {
        message += 'kamu dapat melihat siapa yang menjadi werewolf!!';
        console.log(welcomeMessage);
    } else if (peran.toLowerCase() == 'guard') {
        message += 'kamu akan membantu melindungi temanmu dari serangan werewolf.';
        console.log(welcomeMessage);
    } else if (peran.toLowerCase() == 'werewolf') {
        message += 'Kamu akan memakan mangsa setiap malam!';
        console.log(welcomeMessage);
    }
}
console.log(message);
console.log('\n');

var tanggal = 4;
var bulan = 12;
var tahun = 1890;
var bulanStr = "";
if (tanggal > 31) {
    console.log('Tanggal tidak boleh lebih dari 31');
    return;
} else if (tanggal < 1) {
    console.log('Tanggal tidak boleh kurang dari 1');
    return;
}
if (tahun < 1990) {
    console.log('Tahun tidak boleh kurang dari 1990');
    return;
} else if (tahun > 2200) {
    console.log('Tahun tidak boleh kurang dari 2200');
    return;
}
switch (bulan) {
    case 1:
        bulanStr = 'Januari';
        break;
    case 2:
        bulanStr = 'Februari';
        break;
    case 3:
        bulanStr = 'Maret';
        break;
    case 4:
        bulanStr = 'April';
        break;
    case 5:
        bulanStr = 'Mei';
        break;
    case 6:
        bulanStr = 'Juni';
        break;
    case 7:
        bulanStr = 'Juli';
        break;
    case 8:
        bulanStr = 'Agustus';
        break;
    case 9:
        bulanStr = 'September';
        break;
    case 10:
        bulanStr = 'Oktober';
        break;
    case 11:
        bulanStr = 'November';
        break;
    case 12:
        bulanStr = 'Desember';
        break;
    default:
        console.log('Bulan tidak dapat dikenali');
        return;
}

console.log(tanggal + ' ' + bulanStr + ' ' + tahun);

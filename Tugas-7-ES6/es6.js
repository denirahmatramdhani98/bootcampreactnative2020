//Soal nomer 1
let golden = () => {
    console.log('this is golden');
}
golden();

console.log('\n');
//Soal nomer 2
let newFunction = (namaDepan, namaBelakang) => {
    return {
        namaDepan,
        namaBelakang,
        fullName() {
            console.log(namaDepan + ' ' + namaBelakang);
            return;
        }
    }
}
newFunction('enggar', 'joharnovta').fullName();

console.log('\n');
//Soal nomer 3
let newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
let {
    firstName,
    lastName,
    destination,
    occupation,
    spell
} = newObject;
console.log(firstName, lastName, destination, occupation, spell);

console.log('\n');
//Soal nomer 4
let west = ["Will", "Chris", "Sam", "Holly"];
let east = ["Gill", "Brian", "Noel", "Maggie"];
let combined = [...west,...east];
console.log(combined);

console.log('\n');
//Soal nomer 5
let planet = "earth"
let view = "glass"
var before = `Lorem ${view} dolor sit amet, ` +  
    `consectetur adipiscing elit, ${planet} do eiusmod tempor ` +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
 console.log(before);

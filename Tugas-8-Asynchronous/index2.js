let ReadBooksPromise = require('./promise.js');
let books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

let time = 10000;
let booksLength = books.length;
let Process = (time, index, booksLength) => {
    ReadBooksPromise(time, books[index]).then(function (timeRemaining) {
        time = timeRemaining;
        booksLength = booksLength - 1;
        if (booksLength > 0) {
            Process(time, index + 1, booksLength);
        }
    }).catch(function (error) {

    })
}

Process(time, 0, booksLength);
